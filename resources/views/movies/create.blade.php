@extends('layouts.template')
@section('content')
<div class="col-md 8">
	@if(count($errors))
		<div class="alert alert-danger">
			<ul>
				@foreach($errors->all() as $error)
				<li>{{$error}}</li>
				@endforeach
			</ul>
		</div>
	@endif

	<form action="/upload" method="post" enctype="multipart/form-data">
		<div class="form-group my-3">
			@csrf
			<label>Post Title</label>
			<input type="text" name="title" class="form-control">
			
		</div>

		<div class="form-group">
			<label>Photo:</label>
			<input type="file" name="photo" class="form-control-file">
			
		</div>
		<div class="form-group">
			<label>Post Body</label>
			<textarea name="body" class="form-control" id="summernote"></textarea>
		</div>
		<!-- <div class="form-group">
			<label>Choose Category</label>
			<select name="category" class="form-control">
			<option value="0">Select Category
			</option>
			@foreach($category as $category)
			<option value="{{$category->id}}">
				{{$category->category_name}}
			</option>
			@endforeach
			</select>
		</div> -->
		<div class="form-group">
			<input type="submit" name="btnsubmit" value="Upload" class="btn btn-danger">
		</div>
	</form>
</div>

@endsection